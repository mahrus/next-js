import * as React from 'react';
import Box from '@mui/material/Box';
import SearchIcon from '@mui/icons-material/Search';
export default function BoxSx() {
  const Header = () => {
    return (
      <div
        style={{
          position: 'fixed',
          top:0,
          left:0,
          width: '100%',
          height: '64px',
          backgroundColor: 'white',
          boxShadow: '0px 1px 6px grey',
          display: 'flex',
          justifyContent: 'space-between',
          padding: '0px 32px',
          alignItems: 'center', 
          zIndex:900,
        }}
      >
        <div
          style={{
            background: 'url("https://assets.tokopedia.net/assets-tokopedia-lite/v2/zeus/kratos/e6421cb1.png")',
            width: '179px',
            height: '31px',
            backgroundSize: 'cover'
          }}
        >
        </div>
        <div style={{ display: 'flex', alignItems: 'center', padding: 10 }}>
          <div style={{ display: 'flex' }}>
            <img src="https://assets.tokopedia.net/assets-tokopedia-lite/v2/zeus/kratos/56eb2da3.svg" alt="ic-envelope" />
            <h6 style={{ marginLeft: 12 }}>Pesan Bantuan</h6>
          </div>
          <span style={{
            width: '1px',
            height: '24px',
            backgroundColor: 'grey',
            margin: '0 16px'
          }}></span>
          <div style={{ display: 'flex', alignItems: 'center', padding: 12 }}>
            <img style={{ width: 24, height: 24, borderRadius: '50%' }} src="https://ecs7.tokopedia.net/img/cache/300/default_picture_user/default_toped-13.jpg" /><h6 style={{ marginLeft: 12 }}>Muhammad</h6>
          </div>
        </div>

      </div>
    )
  }
  const SelamatDatang=()=>{
    return (
      <div style={{margin:'auto',display:'flex',flexDirection:'column',marginTop:64}}>
        <div style={{
          background:'url("https://assets.tokopedia.net/assets-tokopedia-lite/v2/zeus/kratos/149df038.svg")',
          width:'100%',
          height: '301px',
          backgroundRepeat:'no-repeat',
          backgroundSize:'100%',
          position:'absolute',
          zIndex:-10,

      }}></div>
        <div style={{paddingLeft:400,paddingRight:400,display:'flex',flexDirection:'row'}}>
            <div style={{marginTop:60}}>
            <h1 style={{fontSize:32,color:'#000000'}}>Selamat Siang, <br/>Ada yang bisa kami bantu?</h1>
           <div style={{display:'flex'}}>
            <input type="text" style={{height:60,width:400, borderWidth:0, borderRadius:4,boxShadow:'0px 1px 6px #00000020',padding: 10,}} placeholder={'Ketik kata kunci (misal:promosi berlangsung)'} />
            <div style={{padding: 10,backgroundColor: '#00000010',width:60, borderTopRightRadius:10,borderBottomRightRadius:10}}>
            <SearchIcon fontSize="large"  />
            </div>
            </div>
           </div>
            
            <img src="https://assets.tokopedia.net/assets-tokopedia-lite/v2/zeus/kratos/6515ba73.png"/>
        </div>
        <div></div>
      </div>
    )
  }
  return (
    <div>
         <Header />
      <SelamatDatang />
    </div>
   
    
  );
}
